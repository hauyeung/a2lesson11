package com.example.a2lesson11;

import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.res.Resources.NotFoundException;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Button startbutton;
	private Button stopbutton;
	private Button prevbutton;
	private Button nextbutton;
	private TextView info; 
	private MediaPlayer mediaplayer;
	private SeekBar seekbar;
	private boolean mWasPlaying;
	private Integer[] clips = {R.raw.persephone_by_snowflake, R.raw.art_now_by_alex_beroza, R.raw.no_time_by_vidian};
	private int currentclip = 0;

	private Handler mHandler = new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		startbutton = (Button) findViewById(R.id.start_button);
		stopbutton = (Button) findViewById(R.id.stop_button);
		prevbutton = (Button) findViewById(R.id.prev_button);
		nextbutton = (Button) findViewById(R.id.next_button);
		info = (TextView) findViewById(R.id.song_info_text);
		startbutton.setEnabled(false);
		stopbutton.setEnabled(false);
		seekbar = (SeekBar) findViewById(R.id.seek_bar);
		
		mediaplayer = new MediaPlayer();
		try
		{
			mediaplayer.setDataSource(getResources().openRawResourceFd(R.raw.persephone_by_snowflake).getFileDescriptor());
			mediaplayer.prepare();
			startbutton.setEnabled(true);
			seekbar.setMax(mediaplayer.getDuration());
			seekbar.setProgress(0);
			seekbar.setOnSeekBarChangeListener(mSeekBarChangeListener);
			mediaplayer.setOnSeekCompleteListener(mMediaPlayerSeekCompleteListener);
			mediaplayer.setOnCompletionListener(mMediaPlayerCompletionListener);
			startbutton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					start();
				}

			});

			stopbutton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					stop();

				}

			});
			
			prevbutton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						stop();
						mediaplayer = MediaPlayer.create(MainActivity.this, clips[--currentclip %3]);
					}
					catch (Exception ex)
					{
						
					}

				}
				
			});
			
			nextbutton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						stop();
						mediaplayer = MediaPlayer.create(MainActivity.this, clips[++currentclip %3]);
					}
					catch (Exception ex)
					{
						
					}

				}
			});
		}
		catch (Exception ex)
		{
			Toast.makeText(this, R.string.error_io_message, Toast.LENGTH_LONG).show();
		}

	}
	


	public void start() {
		mediaplayer.start(); // MediaPlayer is started.
		startbutton.setEnabled(false);
		mHandler.postDelayed(mSeekBarUpdateRunnable, 200);
		stopbutton.setEnabled(true);
	}

	public void stop() {
		mediaplayer.stop(); // MediaPlayer is stopped.
		startbutton.setEnabled(true);
		stopbutton.setEnabled(false);
	}

	private Runnable mSeekBarUpdateRunnable = new Runnable() {
		@Override
		public void run() {
			if (mediaplayer != null) {
				seekbar.setProgress(mediaplayer.getCurrentPosition());
				if (mediaplayer.isPlaying()) {
					mHandler.postDelayed(mSeekBarUpdateRunnable, 200);
				}
			}
		}
	};

	private OnSeekBarChangeListener mSeekBarChangeListener = new OnSeekBarChangeListener() {
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			if (progress != mediaplayer.getCurrentPosition()) {
				mediaplayer.seekTo(progress);
			}
		}
	};

	private OnCompletionListener mMediaPlayerCompletionListener = new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			startbutton.setText(getResources().getString(R.string.start_button_label));
			stopbutton.setEnabled(false);
		}
	};

	private OnSeekCompleteListener mMediaPlayerSeekCompleteListener = new OnSeekCompleteListener() {
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			seekbar.setProgress(mp.getCurrentPosition());
		}
	};


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();

		mWasPlaying = mediaplayer.isPlaying();
		if (mWasPlaying) {
			mediaplayer.pause();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mWasPlaying) {
			start();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putBoolean("isPlaying", mediaplayer.isPlaying());
		outState.putInt("progress", mediaplayer.getCurrentPosition());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		mWasPlaying = savedInstanceState.getBoolean("isPlaying");
		mediaplayer.seekTo(savedInstanceState.getInt("progress"));
		if (!mWasPlaying && savedInstanceState.getInt("progress") > 0) {
			startbutton.setText(getResources().getString(R.string.start_button_label));
		}
	}

}
